import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, switchMap, concatMap, catchError } from 'rxjs/operators';

import * as MovieActions from "./../actions/movie.actions";
import { MovieService } from "./../movies-center/movie.service";


@Injectable()

export class MovieEffects {

  constructor(
    private actions$: Actions,
    private movieService: MovieService
  ) { }



  @Effect() getMovies$: Observable<Action> = this.actions$.pipe(
    ofType<MovieActions.GetMovies>(
      MovieActions.GET_MOVIES
    ),
    switchMap(action => this.movieService.getAllMovies().pipe(
      map(
        items => new MovieActions.GetMoviesDone(items)
      ),
      catchError(error =>
        of(new MovieActions.GetMoviesFailure({ error }))
      )
    ))
  )
  
  @Effect() acceptMovie$: Observable<Action> = this.actions$.pipe(
    ofType<MovieActions.AcceptMovie>(
      MovieActions.ACCEPT_MOVIE
    ),
    concatMap(action => this.movieService.movieAccept(action.payload).pipe(
      map(
        movie => {
          console.log(`Movie id: ${movie.id} - Accepted`);
          return new MovieActions.AcceptMovieDone(movie);
        }),
      catchError(error =>
        of(new MovieActions.AcceptMovieFailure({ error }))
      )
    ))
  )
  
  @Effect() rejectMovie$: Observable<Action> = this.actions$.pipe(
    ofType<MovieActions.RejectMovie>(
      MovieActions.REJECT_MOVIE
    ),
    concatMap(action => this.movieService.movieReject(action.payload).pipe(
      map(
        movie => {
          console.log(`Movie id: ${movie.id} - Rejected`);
          return new MovieActions.RejectMovieDone(movie);
        }),
      catchError(error =>
        of(new MovieActions.RejectMovieFailure({ error }))
      )
    ))
  )




}