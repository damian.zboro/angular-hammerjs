import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Movie } from './../models/movie.model';


export const GET_MOVIES = 'GET_MOVIES';
export const GET_MOVIES_DONE = 'GET_MOVIES_DONE';
export const GET_MOVIES_FAILURE = 'GET_MOVIES_FAILURE';

export const ACCEPT_MOVIE = 'ACCEPT_MOVIE';
export const ACCEPT_MOVIE_DONE = 'ACCEPT_MOVIE_DONE';
export const ACCEPT_MOVIE_FAILURE = 'ACCEPT_MOVIE_FAILURE';

export const REJECT_MOVIE = 'REJECT_MOVIE';
export const REJECT_MOVIE_DONE = 'REJECT_MOVIE_DONE';
export const REJECT_MOVIE_FAILURE = 'REJECT_MOVIE_FAILURE';


export class GetMovies implements Action {
  readonly type = GET_MOVIES;

  constructor() { }
}
export class GetMoviesDone implements Action {
  readonly type = GET_MOVIES_DONE;

  constructor(public payload: Movie[]) { }
}
export class GetMoviesFailure implements Action {
  readonly type = GET_MOVIES_FAILURE;

  constructor(public payload: { error: string }) { }
}


export class AcceptMovie implements Action {
  readonly type = ACCEPT_MOVIE;

  constructor(public payload: Movie) { }
}
export class AcceptMovieDone implements Action {
  readonly type = ACCEPT_MOVIE_DONE;

  constructor(public payload: Movie) { }
}
export class AcceptMovieFailure implements Action {
  readonly type = ACCEPT_MOVIE_FAILURE;

  constructor(public payload: { error: string }) { }
}


export class RejectMovie implements Action {
  readonly type = REJECT_MOVIE;

  constructor(public payload: Movie) { }
}
export class RejectMovieDone implements Action {
  readonly type = REJECT_MOVIE_DONE;

  constructor(public payload: Movie) { }
}
export class RejectMovieFailure implements Action {
  readonly type = REJECT_MOVIE_FAILURE;

  constructor(public payload: { error: string }) { }
}



export type Actions = GetMovies | GetMoviesDone | GetMoviesFailure |
  AcceptMovie | AcceptMovieDone | AcceptMovieFailure |
  RejectMovie | RejectMovieDone | RejectMovieFailure;
  