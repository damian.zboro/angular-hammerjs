import { Action } from '@ngrx/store';
import { Movie } from './../models/movie.model';
import * as MovieActions from './../actions/movie.actions';


export interface State {
  movies: Movie[];
  movies_accept: Movie[];
  movies_reject: Movie[];
  error?: any;
}

const initialState: State = {
  movies: [],
  movies_accept: [],
  movies_reject: [],
  error: null
}


export function reducer(state: State = initialState, action: MovieActions.Actions) {

  switch (action.type) {

    case MovieActions.GET_MOVIES:
      return state;
    case MovieActions.GET_MOVIES_DONE:
      return {
        ...state,
        movies: action.payload
        };
    case MovieActions.GET_MOVIES_FAILURE:
      return {
        ...state,
        error: action.payload.error
      };

    case MovieActions.ACCEPT_MOVIE:
      return state;
    case MovieActions.ACCEPT_MOVIE_DONE:
      return { 
        ...state,
        movies_accept: [...state.movies_accept, action.payload]
      }
    case MovieActions.ACCEPT_MOVIE_FAILURE:
      return {
        ...state,
        error: action.payload.error
      }

    case MovieActions.REJECT_MOVIE:
      return state;
    case MovieActions.REJECT_MOVIE_DONE:
      return { 
        ...state,
        movies_reject: [...state.movies_reject, action.payload]
      }
    case MovieActions.REJECT_MOVIE_FAILURE:
      return {
        ...state,
        error: action.payload.error
      }  


    default:
      return state;
  }

}