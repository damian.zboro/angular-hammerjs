import { Movie } from './../models/movie.model';

export const moviesData: Movie[] = [
  {
    id: '1and3011',
    imageURL: 'https://upload.wikimedia.org/wikipedia/commons/3/37/Gwiezdne_wojny_-_Przebudzenie_Mocy_logo.png', 
    title: 'Inferno',
    summary: 'Lorem​ ​ipsum….',
    rating: 5.3
  },
  {
    id: '2301abc',
    imageURL: 'https://upload.wikimedia.org/wikipedia/commons/f/f1/SWCA_-_Stormtrooper_from_Force_Awakens_%2817202865375%29.jpg',
    title: 'Star​ ​Wars:​ ​Episode​ ​VII​ ​-​ ​The​ ​Force​ ​Awakens',
    summary: 'Lorem​ ​ipsum…dasdas.',
    rating: 8.2
  },
  {
    id: 'x30xabc',
    imageURL: 'https://upload.wikimedia.org/wikipedia/commons/a/ac/Captain_Phasma_%28Star_wars%29.jpg',
    title: 'Kapitan Phasma',
    summary: 'Lorem​ ​ipsum….sadasw',
    rating: 9.6
  },
  {
    id: '0000aaa',
    imageURL: 'https://upload.wikimedia.org/wikipedia/commons/8/8f/Celebration_Anaheim_-_The_Force_Awakens_Exhibit_%28cut%29.jpg',
    title: 'Kylo Ren',
    summary: 'Lorem​ ​ipsum…cxzcxzc',
    rating: 7.7
  }
];