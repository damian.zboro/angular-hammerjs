import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { Movie } from './../models/movie.model';
import { moviesData } from './mock-movies';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class MovieService {

  constructor(private http: HttpClient) { }

  public getAllMovies(): Observable<Movie[]>{
    return of(moviesData);
  }

  public movieAccept(movie: Movie): Observable<Movie>{
    const acceptURL: string = `/recommendations/${movie.id}/accept`;

    //return this.http.put(acceptURL, movie, httpOptions);
    //Dla sprawnego dziłania appki zwracam of()
    return of(movie);
  }

  public movieReject(movie: Movie): Observable<Movie>{
    const rejectURL: string = `/recommendations/${movie.id}/reject`;

    //return this.http.put(rejectURL, movie, httpOptions);
    //Dla sprawnego dziłania appki zwracam of()
    return of(movie);
  }

}