import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { Movie } from './../../models/movie.model';

import { Store } from '@ngrx/store';
import { State } from './../../reducers/movie.reducer';
import * as MovieActions from './../../actions/movie.actions';


import { trigger, keyframes, animate, transition } from '@angular/animations';
import * as kf from './keyframes';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
  animations: [
    trigger('cardAnimator', [
      transition('* => slideOutRight', animate(600, keyframes(kf.slideOutRight))),
      transition('* => slideOutLeft', animate(600, keyframes(kf.slideOutLeft))),
    ])
  ]
})
export class MovieComponent implements OnInit {

  private init: Observable<State>;
  public animationState: string;

  private data: Movie[];
  public movie: Movie = null;
  private movie_index: number = 0;

  constructor(private store: Store<State>) { 
    store.dispatch(new MovieActions.GetMovies());
    this.init = store.select('data');
  }

  ngOnInit() {
    this.init.subscribe(x=>{
      this.data = x.movies;
      this.movie = this.data[this.movie_index];
    })
  }

  public accept(my_movie: Movie): void{

    if(this.movie_index < this.data.length){
      this.store.dispatch(new MovieActions.AcceptMovie(my_movie));

      setTimeout(()=>{
        this.movie_index++;
        this.movie = this.data[this.movie_index];
      }, 600);
    }else{
      this.movie = null;
    }
  }

  public reject(my_movie: Movie): void{

    if(this.movie_index < this.data.length){
      this.store.dispatch(new MovieActions.RejectMovie(my_movie));
      setTimeout(()=>{
        this.movie_index++;
        this.movie = this.data[this.movie_index];
      }, 600);
    }else{
      this.movie = null;
    }
  }


  public startAnimation(state): void {
    if (!this.animationState) {
      this.animationState = state;
    }
  }

  public resetAnimationState(): void {
    this.animationState = '';
  }
}