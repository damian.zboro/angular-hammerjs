import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { MovieComponent } from './movies-center/movie/movie.component';
import { MovieService } from './movies-center/movie.service';
import { CustomMaterialModule } from './shared/custom-material/custom-material.module';

import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers/movie.reducer';
import { EffectsModule } from '@ngrx/effects';
import { MovieEffects } from './effects/movie.effects';

@NgModule({
  imports: [ 
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CustomMaterialModule,
    EffectsModule.forRoot([
      MovieEffects
    ]),
    StoreModule.forRoot({
      data: reducer
    })
  ],
  declarations: [
    AppComponent, 
    MovieComponent
  ],
  providers: [ 
    MovieService 
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
