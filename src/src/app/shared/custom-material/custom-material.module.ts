import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
} from '@angular/material';


const material = [
  CommonModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule
];

@NgModule({
  imports: material,
  exports: material
})
export class CustomMaterialModule {}